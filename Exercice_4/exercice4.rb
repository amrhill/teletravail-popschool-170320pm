#age = nil because it's an optional parameter
def person(first_name, age = nil)
    #if person("Thomas")
    if age == nil
        #Then : je m'appelle Thomas
        puts "Je m’apelle #{first_name}"
    #if person("Thomas", 26)
    else
        #then age has a value and we can print the result to the screen
        puts "Je m’apelle #{first_name}, j'ai #{age} ans"
    end
end