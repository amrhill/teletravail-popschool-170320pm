#array function
def array
    #(1..100) is the range from 1 to 100 we want to have in our array.
    #The to_a allows the array format for the numbers. We have now
    #an array which takes numbers from 1 to 100
    return arr = (1..100).to_a
end

#this function does the same thing, except we didn't force a max number
#we let the choice to put ourselves a max number
def array2(maxNumber)
    #if we call the function like array2(9) it'll prompt the array as [1,2,3,4,5,6,7,8,9]
    return arr2 = (1..maxNumber).to_a
end