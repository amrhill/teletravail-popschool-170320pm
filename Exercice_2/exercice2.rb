#method from exercise 1
def array
    return arr = (1..100).to_a
end

#method partOfArray which return a part the array defined above
def partOfArray
    #array() is the array defined above with values from 1 to 100
    #we select numbers from 30 to 59 with the .select and the conditions between {}
    part = array().select {|r| r > 29 && r < 60}
    return part
end

#this is an easier way to achieve our goal to have an array with values from 29 to 60 from the array
#defined above
def poa_easy
    return poa = array()[28..59]
end