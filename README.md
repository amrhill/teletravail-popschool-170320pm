# 17 03 2020 TELETRAVAIL RUBY POP SCHOOL
> Formateur du jour : David Demonchy

***

> **Exercice_1 :**  
Création d’un tableau avec les chiffres de 1 à 100.  (2 méthodes possibles)  
Une fois créer, afficher chaque valeur. 
 
***

> **Exercice_2 :**  
Depuis le tableau de l’exercice 1, méthode simplifiée,  afficher les nombres compris entre 30 et 59.  

***

> **Exercice_3 :**  
Toujours depuis l’exercice 1  Créer un nouveau tableau avec les nombre paires du tableau de l’exercice_1 en utilisant la méthode «select»  

***

> **Exercice_4 :**  
Ecrire une méthode qui prendra des paramètres ‘first_name’ et ‘age’  
A l’appel de la méthode, celle ci renverra : «Je m’apelle ‘first_name’ , j’ai ‘age’ans »  
Si age n’est pas précisé la chaine renvoi «Je m’apelle ‘first_name’ »